python src/process.py\
   --path_output "processed_18k.png"\
   --path_height "data/world_height_18k.png"\
   --path_water "data/world_water_18k.png"\
   --path_seafloor "data/world_ocean_terrain_18k.png"
