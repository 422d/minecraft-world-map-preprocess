import os
import imageio
import numpy as np
import cv2
import matplotlib.pyplot as plt
import scipy.ndimage as ndimage
import PIL
import time
PIL.Image.MAX_IMAGE_PIXELS = 500000000

def rgb2gray(img):
    """Convert rgb to grayscale, see
    https://en.wikipedia.org/wiki/Grayscale#Luma_coding_in_video_systems
    Inputs:
    - img: np array of shape (w, h, 3)
    Returns:
    - np array shape (w, h)
    """
    r, g, b = img[:,:,0], img[:,:,1], img[:,:,2]
    return 0.2989 * r + 0.5870 * g + 0.1140 * b

def invert(img):
    """Invert colors in image
    Inputs:
    - img: np array of shape (w, h, n)
    Returns:
    - same array inverted and re-written
    """
    return np.invert(img.astype(np.uint), out = img)

def black_white_quantize(img):
    """Perform 1-bit (black, white) quantization of grayscale image.
    Inputs:
    - img: np array of shape (w, h) in uint8 format
    Output:
    - img: new np boolean array
    """
    mid = np.mean(img)
    return np.greater(img, mid)

def generate_mask(path):
    """Create land mask. Land areas will be marke True.
    Note in masked array, True is masked, so for mask array ufuncs we need to use
    ~mask_land (or use mask_sea instead). But for setting values, e.g.
        img[mask_land] = other[mask_land]
    we need to have mask coordinates be True. Hence this mask is used for the
    directly setting/arithmetic operations like above.
    """
    img = imageio.imread(path)
    if len(img.shape) > 2: # rgb image
        img = rgb2gray(img).astype(np.uint8)
    img = black_white_quantize(img)
    return img

def get_mask_edge(mask, thick = 1):
    """Mask edge detection. Return new mask with edge of mask.
    Expands mask outwards (dilation), then does bitwise XOR.
    Inputs:
    - mask: np boolean mask array shape (w, h)
    - thick: number of additional dilations to run to expand mask
    """
    edges = mask ^ ndimage.morphology.binary_dilation(mask)

    # expand edge
    while thick > 0:
        edges = ndimage.morphology.binary_dilation(edges)
        thick -= 1

    return edges

def height_scaling_envelope(debug = False):
    """Height scaling envlope, squashing higher elevations more than lower levels.
    Constrain all heights to a max height.
    """
    MAX_HEIGHT_CLIP = 80
    EXP_CLIP_START = 50
    EXP_CLIP_OFFSET = 7
    EXP_CLIP_DECAY = 45.0

    # height squashing lookup table
    # map height -> new height
    f = np.arange(256)
    
    f[EXP_CLIP_START:] = np.minimum(f[EXP_CLIP_START:], MAX_HEIGHT_CLIP * (1 - np.exp(- (f[EXP_CLIP_START:] - EXP_CLIP_OFFSET) / EXP_CLIP_DECAY )))

    if debug == True:
        print("height squash envelope:", f)
        # import matplotlib.pyplot as plt
        # plt.plot(np.arange(256))
        # plt.plot(f)
        # plt.show()
    
    return f.astype(np.uint8)

def process(
    path_output,
    path,
    path_seafloor,
    mask_land,
    crop_vertical_percents = (0.015, 0.195), # tuple of top and bottom percent of map to cut out before saving
    resize_width = None, # resize width
    apply_seafloor_texture = True,
    sea_level = 40, # target sea level (water level)
    seafloor = 18,  # constant level of ocean under sea
    seafloor_min = 6, # minimum y level for sea floor after applying texture
    seafloor_relief_height = 30, # height variation of seafloor relief texture (seafloor +/- relief/2)
    max_mountain_height = 70,    # max mountain height before squashing
):
    """Process heightmap
    """
    # const params
    MAX_HEIGHT = 255 # max miney man height

    img = imageio.imread(path)
    
    # not rgb, assume grayscale rgb, so choose single channel
    if len(img.shape) > 2:
        img = img[:,:,0]
    
    print("Img size =", img.shape)
    
    # pre-alloc buffers for processing
    img_temp_buf = np.zeros(img.shape, dtype=np.uint8)
    mask_temp_buf = np.zeros(img.shape, dtype=bool)
    mask_temp_buf2 = np.zeros(img.shape, dtype=bool)
    
    # process time start
    t_start = time.process_time()

    # ========================
    # Mask resizing
    # if input mask shape != img shape, resize mask to match img
    # ========================
    if mask_land.shape != img.shape:
        print("Resizing mask", mask_land.shape, "to image shape", img.shape)
        mask_land = cv2.resize(mask_land.astype(np.uint8), (img.shape[1], img.shape[0]), interpolation=cv2.INTER_CUBIC)
        mask_land = np.greater(mask_land, 0)
    
    mask_sea = ~mask_land
    mask_edge = get_mask_edge(mask_land, thick=0)

    # get average height level at edge from land to ocean from mask
    # should be ~12-13 from manually checking in world painter
    avg_sea_wall_height = np.ma.round(np.ma.mean(np.ma.array(img, mask=~mask_edge)))
    print("Average sea wall height =", avg_sea_wall_height)

    # add difference in sea wall height to target sea level,
    # then clip minimum to sea level + 1 (will smooth later)
    img = np.clip(img + (sea_level - avg_sea_wall_height), sea_level + 1, MAX_HEIGHT, out=img)

    # apply sea floor relief height map texture
    # note opencv resize flips (w, h) dimensions relative to numpy
    if apply_seafloor_texture:
        img_seafloor = imageio.imread(path_seafloor)
        if img_seafloor.shape != img.shape:
            img_seafloor = cv2.resize(img_seafloor, dsize=(img.shape[1], img.shape[0]), interpolation=cv2.INTER_CUBIC)
        
        img_seafloor_max = np.ma.max(img_seafloor[mask_sea])
        img_seafloor = img_seafloor.astype(np.float32)
        img_seafloor[mask_sea] /= img_seafloor_max
        img_seafloor[mask_sea] -= 0.5

        img[mask_sea] = seafloor + (img_seafloor[mask_sea] * seafloor_relief_height).astype(np.uint8)

        # clip sea floor
        img = np.clip(img, seafloor_min, MAX_HEIGHT, out=img)

        # apply smoothing
        img[mask_sea] = ndimage.gaussian_filter(img, 1.3, output=img_temp_buf)[mask_sea]
    else:
        img[mask_sea] = seafloor
    
    # sea wall processing: repeatedly expand mask edge and apply following:
    # n iteration: set new outward wall level to `prev - 1`
    # >n iterations: set outward wall level to `prev - 2` until
    # sea floor level reached
    # 
    # so rolloff will be
    # ______ sea level . . . . . . . .
    #       |_        
    #         |_     <- n iterations of 1 roll off/tile
    #           |
    #           |_
    #             |
    #             |
    #          ...
    
    # seawall rolloff function vs. iteration n
    def seawall_rolloff(n):
        if n >= 0 and n < 2:
            return 1
        elif n >= 2 and n < 7:
            return 0
        elif n >= 7 and n < 9:
            return 1
        elif n >= 9 and n < 12:
            return 0
        elif n >= 12 and n < 14:
            return 1
        elif n >= 14 and n < 16:
            return 3
        elif n >= 16 and n < 18:
            return 3
        else:
            return 4

    seawall_height = sea_level - 1
    mask_land_prev = np.zeros(mask_land.shape, dtype=bool)
    mask_land_expanded = mask_land # ref not copy
    iteration = 0
    min_sea_rolloff_level = seafloor - seafloor_relief_height/3
    
    x = False # flag for choosing buffer
    while seawall_height > min_sea_rolloff_level:
        # double buffering ping pong swap to avoid allocs
        if x == False:
            mask_expand_out_buf = mask_temp_buf
            mask_expand_xor_buf = mask_temp_buf2
            x = True
        else:
            mask_expand_out_buf = mask_temp_buf2
            mask_expand_xor_buf = mask_temp_buf
            x = False
        
        np.copyto(mask_land_prev, mask_land_expanded)
        mask_land_expanded = ndimage.morphology.binary_dilation(mask_land_expanded, output=mask_expand_out_buf)
        mask = np.logical_xor(mask_land_prev, mask_land_expanded, out=mask_expand_xor_buf)
        img[mask] = np.ma.maximum(img[mask], seawall_height)

        seawall_height -= seawall_rolloff(iteration)
        iteration += 1

    # smooth sea wall region past land by bit
    img_ocean_smoothed = ndimage.gaussian_filter(img, 1.1, output=img_temp_buf)

    # mask_land_expanded_inner = ndimage.morphology.binary_dilation(mask_land, iterations=1, output=mask_temp_buf)
    mask_land_expanded_outer = ndimage.morphology.binary_dilation(mask_land_expanded, iterations=2, output=mask_temp_buf2)
    mask = np.logical_xor(mask_land, mask_land_expanded_outer, out=mask_temp_buf)
    mask = np.logical_and(mask, np.less(img_ocean_smoothed, sea_level - 3))
    img[mask] = img_ocean_smoothed[mask]

    # ==================================
    # land processing
    # ==================================
    img_original = np.copy(img)

    # global smoothing
    img[mask_land] = ndimage.gaussian_filter(img, 0.75, output=img_temp_buf)[mask_land]

    # apply more aggressive smoothing to higher regions
    mask_highlands = np.greater(img, 60, out=mask_temp_buf)
    img[mask_highlands] = ndimage.gaussian_filter(img, 1.5, output=img_temp_buf)[mask_highlands]

    # squash regions > max mountain level
    # theres some regions not captured in mask_land, so must run on entire image
    f = height_scaling_envelope()
    img = f[img]

    # resolution bumping:
    # mix smoothed layer with noisy base heightmap to create terrain texture
    img[mask_land] = (0.15 * img_original.astype(np.double)[mask_land] + 0.85 * img.astype(np.double)[mask_land]).astype(np.uint8)

    # expand ocean into land region and set coastlines to sea level
    # CURRENTLY do not do, causes bad discontinuities for most lakes in high regions
    # mask_sea_expanded = ndimage.morphology.binary_dilation(mask_sea, iterations=1, output=mask_temp_buf)
    # mask = np.logical_xor(mask_sea, mask_sea_expanded, out=mask_temp_buf)
    # img[mask] = sea_level

    # ==================================
    # cropping
    # ==================================
    # crops off part of antarctica
    crop_top = int(np.round(crop_vertical_percents[0] * img.shape[0]))
    crop_bottom = int(np.round(crop_vertical_percents[1] * img.shape[0]))
    img = img[crop_top:-crop_bottom][:]

    # ==================================
    # final resizing
    # ==================================
    if resize_width != None:
        # resize based on resize width
        resize_scale = resize_width / img.shape[1]
        resize_height = round(img.shape[0] * resize_scale)
        old_shape = img.shape
        img = cv2.resize(img, (resize_width, resize_height), interpolation=cv2.INTER_CUBIC)
        print("Resized: {} -> {}".format(old_shape, img.shape))

    # ==================================
    # save
    # ==================================
    # process time end
    t_end = time.process_time()
    t_process = t_end - t_start

    print("Done processing (time={}s). Saving image... {}".format(t_process, img.shape))

    savepath = os.path.join("build", path_output)
    os.makedirs("build", exist_ok=True)
    imageio.imwrite(savepath, img)
    print("Saved:", savepath)

# https://pvigier.github.io/2018/06/13/perlin-noise-numpy.html
def generate_perlin_noise_2d(shape, res):
    def f(t):
        return 6*t**5 - 15*t**4 + 10*t**3
    
    delta = (res[0] / shape[0], res[1] / shape[1])
    d = (shape[0] // res[0], shape[1] // res[1])
    grid = np.mgrid[0:res[0]:delta[0],0:res[1]:delta[1]].transpose(1, 2, 0) % 1
    # Gradients
    angles = 2*np.pi*np.random.rand(res[0]+1, res[1]+1)
    gradients = np.dstack((np.cos(angles), np.sin(angles)))
    g00 = gradients[0:-1,0:-1].repeat(d[0], 0).repeat(d[1], 1)
    g10 = gradients[1:,0:-1].repeat(d[0], 0).repeat(d[1], 1)
    g01 = gradients[0:-1,1:].repeat(d[0], 0).repeat(d[1], 1)
    g11 = gradients[1:,1:].repeat(d[0], 0).repeat(d[1], 1)
    # Ramps
    n00 = np.sum(grid * g00, 2)
    n10 = np.sum(np.dstack((grid[:,:,0]-1, grid[:,:,1])) * g10, 2)
    n01 = np.sum(np.dstack((grid[:,:,0], grid[:,:,1]-1)) * g01, 2)
    n11 = np.sum(np.dstack((grid[:,:,0]-1, grid[:,:,1]-1)) * g11, 2)
    # Interpolation
    t = f(grid)
    n0 = n00*(1-t[:,:,0]) + t[:,:,0]*n10
    n1 = n01*(1-t[:,:,0]) + t[:,:,0]*n11
    return np.sqrt(2)*((1-t[:,:,1])*n0 + t[:,:,1]*n1)

# https://pvigier.github.io/2018/06/13/perlin-noise-numpy.html
def generate_fractal_noise_2d(shape, res, octaves=1, persistence=0.5):
    noise = np.zeros(shape)
    frequency = 1
    amplitude = 1
    for _ in range(octaves):
        noise += amplitude * generate_perlin_noise_2d(shape, (frequency*res[0], frequency*res[1]))
        frequency *= 2
        amplitude *= persistence
    return noise


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description="Process earth height map data for world painter")
    
    parser.add_argument(
        "--path_output",
        dest="path_output",
        required=True,
        action="store",
        type = str,
        help="Output file name")
    parser.add_argument(
        "--path_height",
        dest="path_height",
        required=True,
        action="store",
        type = str,
        help="Height map path")
    parser.add_argument(
        "--path_water",
        dest="path_water",
        required=True,
        action="store",
        type = str,
        help="Water mask path")
    parser.add_argument(
        "--path_seafloor",
        dest="path_seafloor",
        required=True,
        action="store",
        type = str,
        help="Seafloor height map path")
    parser.add_argument(
        "--resize_width",
        dest="resize_width",
        required=False,
        action="store",
        type = int,
        help="Resized width of map before export")
    parser.add_argument(
        "--debug",
        dest="debug",
        action="store_true",
        help="Debug stuff")
    
    args = parser.parse_args()

    # unpack
    path_output = args.path_output
    path_height = args.path_height
    path_water = args.path_water
    path_seafloor = args.path_seafloor
    resize_width = args.resize_width
    debug = args.debug

    np.random.seed(69)

    mask_land = generate_mask(path_water)
    
    # debug masks
    if debug:
        imageio.imwrite("build/mask_land.png", mask_land.astype(np.uint8) * 255)
        imageio.imwrite("build/mask_sea.png", (~mask_land).astype(np.uint8) * 255)
        imageio.imwrite("build/mask_edge.png", get_mask_edge(mask_land, thick = 0).astype(np.uint8) * 255)
        f = height_scaling_envelope(debug=True)

    processed = process(
        path_output,
        path_height,
        path_seafloor,
        mask_land,
        resize_width=resize_width,
    )
